document.addEventListener('DOMContentLoaded', () => {
    onLoad()
});

function registerServiceWorker() {
    return navigator.serviceWorker.register('service-worker.js');
}

function getPushManagerInstance() {
    return navigator.serviceWorker.ready
        .then((registration) => {
            return registration.pushManager;
        });
}

function getPushSubscriptionInstance() {
    return getPushManagerInstance()
        .then((pushManager) => {
            return pushManager.getSubscription()
        })
}

function onLoad() {
    if (!(navigator.serviceWorker && 'PushManager' in window)) {
        // service worker is not supported, so it won't work!
        updateUiState('not_supported')
        return;
    }

    registerServiceWorker().then(() => {
        updateUiState('sw_registered');
        initRegisterPushBtn();
    }).catch(err => {
        updateUiState('error', err);
    })
}

function initRegisterPushBtn() {
    const registerPushBtn = document.getElementById('register-push');

    registerPushBtn.addEventListener('click', (event) => {
        event.preventDefault();
        updateUiState('registering_push')
        registerPush().then(() => {
            updateUiState('push_registered')
            initSendPushBtn()
            initUnregisterBtn()
        }).catch(error => {
            updateUiState('error', error)
        })
    });
}

function initUnregisterBtn() {
    const unsubPushBtn = document.getElementById('unsubscribe-push');

    getPushSubscriptionInstance().then(() => {
        unsubPushBtn.addEventListener('click', (event) => {
            event.preventDefault();
            unsubscribePush().then(() => {
                updateUiState('sw_registered');
            });
        });
    })
}

function initSendPushBtn() {
    const sendPushBtn = document.getElementById('send-push');

    sendPushBtn.addEventListener('click', (event) => {
        event.preventDefault();
        sendPush()
    });
}

function registerPush() {
    return getPushManagerInstance().then(pushManager => {
        pushManager.getSubscription().then((subscription) => {
            if (subscription) {
                // renew subscription if we're within 5 days of expiration
                const fiveDaysMilliseconds = 432000000
                if (subscription.expirationTime && Date.now() > subscription.expirationTime - fiveDaysMilliseconds) {
                    unsubscribePush().then(() => {
                        subscribePush(pushManager);
                    });
                }
            }

            subscribePush(pushManager);
        });
    })
}

function sendPush() {
    getPushSubscriptionInstance().then(sub => {
        apiSendPush(sub, 'Testing push messages',
            'Click on this notification', 5000);
    })
}

function subscribePush(pushManager) {
    apiGetKey().then((key) => {
        pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: key
        });
    });
}

function unsubscribePush() {
    return getPushSubscriptionInstance().then(sub => {
        return sub.unsubscribe();
    })
}

function updateUiState(stateName, errorMessage = null) {
    const registerPushBtn = document.getElementById('register-push');
    const unsubPushBtn = document.getElementById('unsubscribe-push');
    const sendPushBtn = document.getElementById('send-push');
    const messageBoard = document.getElementById('message-board')

    messageBoard.innerHTML = "Updating state"
    registerPushBtn.setAttribute('disabled', 'disabled');
    registerPushBtn.innerText = '';
    unsubPushBtn.setAttribute('disabled', 'disabled');
    unsubPushBtn.innerText = '';
    sendPushBtn.setAttribute('disabled', 'disabled');
    sendPushBtn.innerText = '';

    if (stateName === 'error') {
        messageBoard.innerHTML = "Error: " + JSON.stringify(errorMessage)
    }

    if (stateName === 'not_supported') {
        messageBoard.innerHTML = 'SW & Push are Not Supported'
    }

    if (stateName === 'registering_push') {
        messageBoard.innerHTML = 'Attempting to register push'
    }

    if (stateName === 'sw_registered') {
        messageBoard.innerHTML = 'Service worker registered'
        registerPushBtn.removeAttribute('disabled')
        registerPushBtn.innerText = 'Register push';

        /* remove click event listeners */
        unsubPushBtn.replaceWith(unsubPushBtn.cloneNode(true));
        sendPushBtn.replaceWith(sendPushBtn.cloneNode(true));
    }

    if (stateName === 'push_registered') {
        messageBoard.innerHTML = 'Push registered'
        registerPushBtn.innerText = 'Push registered';
        unsubPushBtn.innerText = 'Unregister push';
        unsubPushBtn.removeAttribute('disabled')
        sendPushBtn.innerText = 'Send push';
        sendPushBtn.removeAttribute('disabled')
    }
}
