
require('dotenv').config();

const https = require("https");
const fs = require("fs");
const express = require('express');
const bodyParser = require('body-parser');
const configuredWebPush = require('./configured-web-push');

const app = express();
const envListenPort = process.env.ENV_LISTEN_PORT || 80;
const staticFolder = process.env.ENV_NODE_ENV === 'production' ? 'dist' : 'src';

let envLetsencryptCertsPath = process.env.ENV_LETSENCRYPT_CERTS_PATH || '';
/* strip last slash */
envLetsencryptCertsPath = envLetsencryptCertsPath.replace(/\/$/, '')

const envHttpsMode = process.env.ENV_HTTPS_MODE === 'true' ? true : false;

// Express configuration
app.disable('x-powered-by');

app.use(express.static(staticFolder));
app.use(bodyParser.json());

app.get('/api/key', function (req, res) {
    if (configuredWebPush.vapidPublicKey !== '') {
        res.send({
            key: configuredWebPush.vapidPublicKey
        });
    } else {
        res.status(500).send({
            key: 'VAPID KEYS ARE NOT SET'
        });
    }
});

app.post('/api/notify', async function (req, res) {
    const data = req.body;
    console.log('subscription', data.subscription)

    const sendNotification = async function () {
        try {
            await configuredWebPush.webPush.sendNotification(data.subscription, data.payload, { contentEncoding: data.encoding })
                .then(function (response) {
                    console.log('Response: ' + JSON.stringify(response, null, 4));
                    res.status(201).send(response);
                })
                .catch(function (e) {
                    console.log('E', e, JSON.stringify(e))
                    console.log('Error: ' + JSON.stringify(e, null, 4));
                    res.status(201).send(e);
                });
        } catch (e) {
            res.status(500).send(e.message);
        }
    };

    if (data.delay) {
        setTimeout(sendNotification, data.delay);
    } else {
        sendNotification();
    }
});

if (envHttpsMode) {
    https
        .createServer(
            // Provide the private and public key to the server by reading each
            // file's content with the readFileSync() method.
            {
                key: fs.readFileSync(`${envLetsencryptCertsPath}/privkey.pem`),
                cert: fs.readFileSync(`${envLetsencryptCertsPath}/cert.pem`),
                ca: fs.readFileSync(`${envLetsencryptCertsPath}/chain.pem`),
            },
            app
        ).listen(envListenPort, function () {
            console.log(`Server listening on port ${envListenPort}`);
        });
} else {
    app.listen(envListenPort, function () {
        console.log(`Server listening on port ${envListenPort}`);
    });
}
