const webPush = require('web-push');

const vapidPublicKey = process.env.ENV_VAPID_PUBLIC_KEY || '';
const vapidPrivateKey = process.env.ENV_VAPID_PRIVATE_KEY || '';

if (vapidPublicKey === '' || vapidPrivateKey === '') {
    console.log("You must set the ENV_VAPID_PUBLIC_KEY and ENV_VAPID_PRIVATE_KEY " +
      "environment variables in .env file. You can use the following ones:");
    console.log(webPush.generateVAPIDKeys());
    return;
} else {
    webPush.setVapidDetails(
        'mailto:email@gmail.com',
        vapidPublicKey,
        vapidPrivateKey
    );
}

module.exports = {
    webPush: webPush,
    vapidPublicKey: vapidPublicKey
}
